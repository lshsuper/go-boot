package http_provider

import "time"

const (
	IdleConnTimeout     = 90 * time.Second
	MaxIdleConnsPerHost = 1000
	Timeout             = time.Minute * 1
	defaultTag          = "default-tag"
)
