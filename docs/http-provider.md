### Http Client

**带有链接池的http-client**

#### 一、基本使用

##### Step1：构建连接池对象

```
	client := NewHttpClient(HttpClientConf{})  //全局使用
```

##### Step2：简单发送请求

```
	str, _ := client.DoGet("https://www.baidu.com").StrResp()
```



#### 二、工厂化管理连接池对象


##### Step1：注册连接池对象

```
       Use(HttpClientConf{Tag:"你的对象标识"})   //程序启动注册

```

##### Step2：获取连接池对象

```
       client:=Get("你的对象标识")   //程序启动注册

```



