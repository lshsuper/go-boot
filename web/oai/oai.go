package oai

type Info struct {
	Title       string `json:"title"`
	Description string `json:"description"`
	Version     string `json:"version"`
}

//Paths 接口路径
type Paths map[string]PathsElem

//PathsElem 接口路径信息体
type PathsElem map[string]PathsElemInfo

type PathsElemInfoType int

const (
	_ PathsElemInfoType = iota
	MethodInfo
	SummaryInfo
)

//PathsElemInfo 接口信息详情
type PathsElemInfo interface {
	Type() PathsElemInfoType
}

//PathsElemMethodInfo 请求方法体信息
type PathsElemMethodInfo struct {
	Summary     string                                 `json:"summary"`
	Tags        []string                               `json:"tags"`
	Responses   map[string]PathsElemMethodInfoResponse `json:"responses"`
	Parameters  []PathsElemMethodInfoParameter         `json:"parameters"`
	RequestBody PathsElemMethodInfoRequestBody         `json:"requestBody"`
}
type PathsElemMethodInfoRequestBody struct {
	Required bool                                             `json:"required"`
	Content  map[string]PathsElemMethodInfoRequestBodyContent `json:"content"`
}

type PathsElemMethodInfoRequestBodyContent struct {
	Schema PathsElemMethodInfoRequestBodyContentSchema
}

type PathsElemMethodInfoRequestBodyContentSchema struct {
	Schema PathsElemMethodInfoResponseContentSchema `json:"schema"`
}

type PathsElemMethodInfoParameter struct {
	Description string                             `json:"description"`
	In          string                             `json:"in"`
	Name        string                             `json:"name"`
	Schema      PathsElemMethodInfoParameterSchema `json:"schema"`
	Required    bool                               `json:"required"`
}

type PathsElemMethodInfoParameterSchema struct {
	Description string   `json:"description"`
	Format      string   `json:"format"`
	Properties  []string `json:"properties"`
	Type        string   `json:"type"`
}

type PathsElemMethodInfoResponse struct {
	Content     map[string]PathsElemMethodInfoResponseContent `json:"content"`
	Description string                                        `json:"description"`
}
type PathsElemMethodInfoResponseContent struct {
	Schema PathsElemMethodInfoResponseContentSchema `json:"schema"`
}

type PathsElemMethodInfoResponseContentSchema struct {
	Ref string `json:"$ref"`
}

func (info PathsElemMethodInfo) Type() PathsElemInfoType {
	return MethodInfo
}

//PathsElemSummaryInfo  描述信息
type PathsElemSummaryInfo string

func (info PathsElemSummaryInfo) Type() PathsElemInfoType {
	return SummaryInfo
}
