package rpc_provider

import "sync"

var fatory = NewRpcPoolFactory()

type rpcPoolFactory struct {
	pMap map[string]*grpcPool
	rw   *sync.RWMutex
}

func NewRpcPoolFactory() *rpcPoolFactory {
	return &rpcPoolFactory{
		pMap: make(map[string]*grpcPool),
		rw:   new(sync.RWMutex),
	}
}

func (factory *rpcPoolFactory) UseGrpc(cnf GrpcPoolConf) {

	defer factory.rw.Unlock()
	factory.rw.Lock()
	factory.pMap[cnf.Tag] = NewGrpcPool(cnf)
	return

}

func (factory *rpcPoolFactory) Get(tag string) *grpcPool {

	defer factory.rw.RUnlock()
	factory.rw.RLock()
	return factory.pMap[tag]

}

func UseGrpcClient(cnf GrpcPoolConf) {

	fatory.UseGrpc(cnf)

}

func GrpcClient(tag string) (*grpcClient, error) {

	return fatory.Get(tag).Get()

}
