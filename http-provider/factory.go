package http_provider

import "sync"

var factory *httpClientFactory = NewHttpClientFactory()

type httpClientFactory struct {
	rw   *sync.RWMutex
	cMap map[string]*httpClient
}

//NewHttpClientFactory 构建
func NewHttpClientFactory() *httpClientFactory {
	return &httpClientFactory{
		rw:   new(sync.RWMutex),
		cMap: make(map[string]*httpClient),
	}
}

//Use 注册启用链接对象
func (factory *httpClientFactory) Use(conf HttpClientConf) *httpClientFactory {
	defer factory.rw.Unlock()
	factory.rw.Lock()
	factory.cMap[conf.Tag] = NewHttpClient(conf)
	return factory
}

//Get 获取连接池对象
func (factory *httpClientFactory) Get(tag string) *httpClient {

	defer factory.rw.RUnlock()
	factory.rw.RLock()
	return factory.cMap[tag]

}

//=======default factory =============

//Use 注册启用链接对象
func Use(conf HttpClientConf) *httpClientFactory {
	if len(conf.Tag) <= 0 {
		conf.Tag = defaultTag
	}
	return factory.Use(conf)

}

//Get 获取连接池对象
func Get(tag ...string) *httpClient {
	if tag == nil {
		return factory.Get(defaultTag)
	}
	return factory.Get(tag[0])
}
