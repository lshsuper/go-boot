package http_provider

import (
	"net/http"
	"sync"
	"testing"
)

//TestDo 测试使用
func TestDo(t *testing.T) {
	//构建实例
	client := NewHttpClient(HttpClientConf{
		MaxIdleConnsPerHost: 2000,
	})
	wg := &sync.WaitGroup{}
	for i := 0; i < 1000; i++ {

		wg.Add(1)

		go func() {

			defer func() {
				wg.Done()
				client.Close()
			}()

			_, err := client.DoGet("https://www.sohu.com/").StrResp()
			if err != nil {
				t.Log(err.Error())
			}

		}()
	}

	wg.Wait()

}

//TestSingleDo 测试使用
func TestSingleDo(t *testing.T) {

	wg := &sync.WaitGroup{}
	for i := 0; i < 1000; i++ {

		wg.Add(1)

		go func() {

			defer func() {
				wg.Done()

			}()
			client := &http.Client{}

			_, err := client.Get("https://www.sohu.com/")
			if err != nil {
				t.Log(err.Error())
			}

		}()
	}

	wg.Wait()

}
