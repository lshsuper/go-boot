# go-boot

#### 介绍

类似于java的spring-boot/c#的surging/go的goframe,期望可以通过迭代使其具有更多的能力，包括相关脚手架的构建

#### 规范说明
1. 脚手架（app-cli）所启动的工具库版本基于define/const->Version控制，代码分支打tag的版本要与该字段保持一致
2. 每一种工具包另起新包定义，存在多种解决方案的工具，尽量采用类似适配器模式进行编写（只暴露用户多可用接口，具体是那种策略，由内部适配）

#### 软件架构

grpc cli gin gorm ....

#### 安装教程

```
 go get gitee.com/lshsuper/go-pkg@对应版本
```

#### 组件文档
> [对象存储工厂](./docs/file-store.md)

> [数据库上下文](./docs/database.md)

> [excel操作](./docs/excel.md)

> [grpc操作](./docs/rpc-provider.md)

> [web框架](./docs/web.md)

> [web工程脚手架](./docs/app-cli.md)

> [Http Client](./docs/http-provider.md)




