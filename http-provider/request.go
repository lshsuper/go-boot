package http_provider

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
)

//httpRequest http请求体
type httpRequest struct {
	c      *http.Client
	url    string
	method string
	r      *http.Request
	err    error
}

//newHttpRequest 构建请求
func newHttpRequest(c *http.Client, url, method string) *httpRequest {
	r, _ := http.NewRequest(method, url, nil)
	return &httpRequest{
		r:      r,
		method: method,
		url:    url,
		c:      c,
	}

}

//Error 获取错误信息
func (req *httpRequest) Error() error {
	return req.err
}

//WithCookie 携带cookie
func (req *httpRequest) WithCookie(name, val string) *httpRequest {
	req.r.AddCookie(&http.Cookie{
		Name:  name,
		Value: val,
	})
	return req
}

//WithHeader 携带header
func (req *httpRequest) WithHeader(key, val string) *httpRequest {
	req.r.Header.Add(key, val)
	return req
}

//WithFormData 携带form
func (req *httpRequest) WithFormData(key, val string) *httpRequest {
	req.r.PostForm.Set(key, val)
	return req
}

//WithJson 携带json
func (req *httpRequest) WithJson(body interface{}) *httpRequest {
	by, err := json.Marshal(body)
	if err != nil {
		req.err = err
		return req
	}

	req.r.Header.Add("Content-Type", "application/json; charset=utf-8")
	req.r.Body = io.NopCloser(bytes.NewBuffer(by))

	return req
}

//Resp 原生结果
func (req *httpRequest) Resp() (res *http.Response, err error) {

	if req.err != nil {
		err = req.err
		return
	}

	res, err = req.c.Do(req.r)

	return
}

//ByteResp byte message
func (req *httpRequest) ByteResp() (by []byte, err error) {
	r, e := req.Resp()
	if e != nil {
		err = e
		return
	}
	by, err = ioutil.ReadAll(r.Body)
	r.Body.Close()
	return
}

//StrResp str message
func (req *httpRequest) StrResp() (res string, err error) {

	by, e := req.ByteResp()
	if e != nil {
		err = e
		return
	}

	if len(by) <= 0 {
		return
	}
	res = string(by)
	return

}

//JsonResp json message
func (req *httpRequest) JsonResp(res interface{}) (err error) {

	by, e := req.ByteResp()
	if e != nil {
		err = e
		return
	}

	if len(by) <= 0 {
		return
	}

	err = json.Unmarshal(by, res)
	return

}
