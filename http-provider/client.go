package http_provider

import (
	"crypto/tls"
	"net"
	"net/http"
	"time"
)

type httpClient struct {
	c *http.Client
}

type HttpClientConf struct {
	MaxIdleConnsPerHost int           //单host最大空闲连接数
	IdleConnTimeout     time.Duration //空闲连接存活时间
	Timeout             time.Duration //请求超时时间
	Tag                 string
}

func NewHttpClient(conf HttpClientConf) *httpClient {

	if conf.IdleConnTimeout <= 0 {
		conf.IdleConnTimeout = IdleConnTimeout
	}

	if conf.MaxIdleConnsPerHost <= 0 {
		conf.MaxIdleConnsPerHost = MaxIdleConnsPerHost
	}

	if conf.Timeout <= 0 {
		conf.Timeout = Timeout
	}

	//实例化
	client := &httpClient{}
	client.c = &http.Client{
		Timeout: conf.Timeout, //设置超时时间
		Transport: &http.Transport{
			Proxy: http.ProxyFromEnvironment,
			DialContext: (&net.Dialer{
				Timeout:   30 * time.Second,
				KeepAlive: 30 * time.Second,
			}).DialContext,
			ForceAttemptHTTP2:     true,
			MaxIdleConnsPerHost:   conf.MaxIdleConnsPerHost,
			IdleConnTimeout:       conf.IdleConnTimeout,
			TLSHandshakeTimeout:   10 * time.Second,
			ExpectContinueTimeout: 1 * time.Second,
			TLSClientConfig:       &tls.Config{InsecureSkipVerify: true},
		},
	}

	return client
}

//DoRequest 构建请求
func (client *httpClient) DoRequest(method, url string) *httpRequest {

	req := newHttpRequest(client.c, url, method)
	return req

}

//DoGet 构建get请求
func (client *httpClient) DoGet(url string) *httpRequest {

	return client.DoRequest(http.MethodGet, url)

}

//DoPost 构建post请求
func (client *httpClient) DoPost(url string) *httpRequest {

	return client.DoRequest(http.MethodPost, url)

}

//DoPut 构建put请求
func (client *httpClient) DoPut(url string) *httpRequest {

	return client.DoRequest(http.MethodPut, url)

}

//DoDelete 构建delete请求
func (client *httpClient) DoDelete(url string) *httpRequest {

	return client.DoRequest(http.MethodDelete, url)

}

//DoPatch 构建patch请求
func (client *httpClient) DoPatch(url string) *httpRequest {
	return client.DoRequest(http.MethodPatch, url)
}

//Close 销毁
func (client *httpClient) Close() {
	client.c.CloseIdleConnections()

}
